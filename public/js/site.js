$(document).ready(function() 
{
    site.init();
});

var site = 
{
    init:function()
    {
        // Only execute javascript that the controller requires
        var controller = $('body').attr('id');
        switch(controller)
        {
            case 'index':
                user.init();
                form.init();
                break;
            case 'wiki':
                wiki.init();
                form.init();
        }
    }
    
}


var wiki = 
{
    init:function()
    {
        $('#addEntry').click(function()
        {
             $( "#addEntryContainer" ).slideDown( "slow");
        });  
        $('#addCategory').click(function()
        {
            wiki.addCategory();
        }); 
       
    },
    addCategory: function()
    {
        ajax.request('/wiki/addcategory', 
        null,
        'html', 
        'GET',
        function(response)
        {
            $('#formInjection').html(response);
            $('#formInjection').fadeIn('slow');
            $('html, body').animate({
                    scrollTop: $("#formInjection").offset().top
                }, 2000);
            form.init();
        });
    }  
}

var form = 
{
    init:function()
    {
        $('.ajaxSubmit').on("click", function()
        {
            var form = $(this).closest("form");
            ajax.form(form);
        });
    },
    
    registerCallback:function()
    {
        $('.formContainer').fadeOut(function()
        {
            $('.thankYouContainer').fadeIn();
        });
        
    },
    
    loginCallback:function()
    {
        $('.formContainer').fadeOut(function()
        {
            window.location.href='/wiki';
        });
    }
    
}

var user = 
{
    init:function()
    {
        $('.createAccount').click(function()
        {
            user.register();
        });  
        $('#login').click(function()
        {
            user.login();
        });   
    },
    login: function()
    {
        ajax.request('/user/login', 
        null,
        'html', 
        'GET',
        function(response)
        {
            $('#formInjection').html(response);
            $('#formInjection').fadeIn('slow');
            $('#createAccountContainer').hide();
            $('.infoContainer').hide();
            form.init();
        });
    },
    register: function()
    {
        ajax.request('/user/register', 
        null,
        'html', 
        'GET',
        function(response)
        {
            $('#formInjection').html(response);
            $('#formInjection').fadeIn('slow');
            $('#createAccountContainer').hide();
            $('.infoContainer').hide();
            form.init();
        });
    }
}