ajax =
{
    form: function(form)
    {
        var formId = form.attr('id');
        var formName = form.attr('name');
        var formElements = [];
        $('#' + formId + ' input').each(function()
        {
            if($(this).attr('name') != undefined)
            {
                
                formElements[$(this).attr('name')] = $(this).val();
                console.log(formElements);
            }
        });
        ajax.request($(form).attr('action'), 
        formElements,
        'json', 
        'POST', 
        function(response)
        {
            window["form"][formName + 'Callback']();
        });

    },

    request: function(url, params, datatype, method, callback, errorcallback)
    {            
        if(datatype == null)
        {
            datatype = 'json';
        }
        if(method == null)
        {
            method = 'POST';
        }
        var paramString = '';
        if(params != null)
        {
            for (var k in params)
            {
                if(params.hasOwnProperty(k)) 
                {   
                    paramString += '&' + k + '=' + params[k];
                }
            }
            paramString = paramString.substring(1);
            
        }
        $.ajax({
           type: method,
           url: url,
           data: paramString,
           dataType: datatype,
           success: function(response)
           {
               
               if(datatype == 'json')
               {
                   switch(response['success'])
                   {
                        //Success
                        case 1:
                            ajax.messageInjection(response['notifications']);
                            callback(response['data'][0]);
                            break;
                        //Fail
                        default:
                            ajax.messageInjection(response['notifications']);
                            break;
                   }
               }
               else
               {
                   callback(response);
               } 
           },
           error: function(xhr, textStatus, errorThrown)
           {
                ajax.fatalError(this, textStatus, errorThrown);
           }
         });
    },
    
    messageInjection: function(notifications)
    {
        notification.ajax(notifications);
    },
    
    fatalError: function(xhr, textStatus, errorThrown)
    {
        var errorText;
        if(xhr.statusText == undefined)
        {
            errorText = 'An unexpected error occurred. Please try again.';
        }
        else
        {
            errorText = xhr.statusText;
        }
        var error = [];
        error[0] = [];
        error[0]['type'] = 'Fatal';
        error[0]['message'] = errorText;
        notification.ajax(error);
    }
}

general = 
{
    refreshPage: function()
    {
        $('.refreshPage').click(function()
        {
            window.location = document.URL;
        });
    },

    getId: function(element)
    {
        return $(element).attr('id').split('_')[1];
    },

    clearOnClick: function()
    {
        $('.clearOnClick').each(function()
        {
            var startValue = $(this).val();
            $(this).focus(function()
            {
                if($(this).val() == startValue)
                {
                    $(this).val('');
                }
                else if($(this).val() == '')
                {
                    $(this).val(startValue);
                }
            });
            $(this).blur(function()
            {
                if($(this).val() == '')
                {
                    $(this).val(startValue);
                }
            });
        });
    }
    
}

notification = 
{
    reset: function()
    {
        $('#messagesContainer').html('');
    },
    ajax: function(notifications)
    {
        if(notifications.length == 0)
        {
            notifications[0] = "An error occurred. Please try again.";
        }
        var notificationHtml = "";
        for(i=0;i<notifications.length;i++)
        {
            if(notifications[i].type != undefined)
            {
                notificationHtml = notificationHtml + "<li class='" + 
                    notifications[i].type.toLowerCase() + "Notification'>" + notifications[i].message 
                    + "</li>";
            }
            
        }
        $('#messagesContainer').html("<ul>" + notificationHtml + "</ul>");
        
    }
}