<?php

namespace seewiki;

abstract class request
{
    /**
     * Check if data has been posted
     * @return type 
     */
    public function post()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            return true;
        }
    }
    
    /**
     * Get post params
     * @return type 
     */
    public function getParams()
    {
        return $_POST;
    }
}

?>
