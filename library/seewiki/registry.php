<?php
/**
 * The Registry class
 * Implements the Registry and Singleton design patterns
 *
 * @version 0.1
 * @author David Bainbridge
 * @since 19/06/10
 * */

namespace seewiki;

class registry
{
	private static $instance;
	private static $values = array();
	private static $objects = array();
	
	private function __construct() {}
	
	/**
	 * Singleton method used to access object avoiding duplication
	 * @return object
	 **/
	static function getinstance()
	{
	    if(!isset(self::$instance))
	    {
		self::$instance = new self();
	    }
	    return self::$instance;
	}
	
	/**
	 * Prevent duplication of this object
	 **/
	public function __clone()
	{
            //todo using trigger_error?
	    trigger_error('Cloning the registry is not permitted.',E_USER_ERROR);
	}
	
	/**
	 * Return a stored value
	 * @return string
	 * */
	public function get($key)
	{
	    if(isset($this->values[$key]))
	    {
		return $this->values[$key];
	    }
	    return null;
	}
	
	/**
	 * Store a value in the registry
	 * @param string $key
	 * @param string $value
	 * */
	public function set($key,$value)
	{
	    $this->values[$key] = $value;
	}
	
	
	/**
         * TODO UPDATE OR REMOVE
	 * Store an object in the registry
	 * @param object $object name of the object to store
	 * @param string $key access title for the stored object
	 **/
	public function storeObject($object, $key)
	{
	    require_once(ROOT . DS . 'library' . DS . $object . 'Class.php');
	    self::$objects[$key] = new $object(self::$instance);
	}
	
	
	/**
         * TODO UPDATE OR REMOVE
	 * Get Object stored in registry
	 * @param string $key name of the object
	 * @return object
	 **/
	public function getObject($key)
	{
	    if(is_object(self::$objects[$key]))
	    {
		return self::$objects[$key];
	    }
	}
        
        /**
         * Check if item is set in the registry
         * @param string $key
         * @return bool 
         */
        public function isRegistered($key)
        {
            return isset($this->values[$key]);
        }
}