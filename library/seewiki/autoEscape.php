<?php
/**
 * Auto escaping for view variables
 */
namespace seewiki;

class autoEscape 
{
    
   /**
    * Escapes strings and recursively iterates over arrays to escape all entries.
    *
    * @param mixed $original
    * @return mixed
    */
    public function escapeDeep($original) 
    {
        if (is_string($original))
        {
            return htmlspecialchars($original, ENT_QUOTES);
        }
        elseif (is_array($original)) 
        {
            foreach ($original as $k => $v) 
            {
                $original[$k] = $this->escapeDeep($v);
            }
        }
        return $original;
    }
}

?>