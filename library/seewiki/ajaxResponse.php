<?php

namespace seewiki;

class ajaxResponse
{
    protected $_success = false;
    protected $_notifications = array();
    protected $_data = array();
    
    /**
     * Set the success of an ajax call
     * @param bool $success 
     */
    public function setSuccess($success)
    {
        $this->_success = $success;
        if($success == false)
        {
            $this->setNotifications();
        }
    }
    
    /**
     * Add any notifications set to the ajax response
     */
    private function setNotifications()
    {
        // Get any notifications from the registry
        $registry = registry::getInstance();
        $notifications = $registry->get('notifications');
        
        if($notifications)
        {
            foreach($notifications as $notification)
            {
                $this->addNotification($notification->getType(), $notification->getMessage());
            }
        }
    }
    
    /**
     * Add a notification to the ajax response
     * @param string $type
     * @param string $message
     */
    public function addNotification($type, $message)
    {
        $this->_notifications[] = array('type'=>$type,'message'=>$message);
    }
    
    /**
     * Add data to the ajax response
     * @param type $data 
     */
    public function addData($key, $value)
    {
        $this->_data[$key] = $value;
    }
    
    /**
     * Echo out the json response
     */
    public function outputResponse()
    {
        $data['success'] = (int)$this->_success;
        $data['notifications'] = $this->_notifications;
        $data['data'] = $this->_data;
        echo json_encode($data);
    }
}
?>
