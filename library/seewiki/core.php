<?php

namespace seewiki;

abstract class core
{
    /**
     * Return alphanumerical string / array
     * @param mixed $string
     * @param bool $extra
     * @return string 
     */
    public function alnum($vars, $extra=false)
    {
        if(is_array($vars))
        {
            foreach($vars as $var)
            {
                $vars[$var] = $this->alnum($var, $extra);
            }
        }
        if($extra == true)
        {
            return $vars;
            return preg_replace("/[^a-zA-Z0-9 !@#$%&.,]/","",$vars);
        }
        else
        {
            return $vars;
            return preg_replace("/[^a-zA-Z0-9]+/", "", $vars);
        }
    }
    
}

?>
