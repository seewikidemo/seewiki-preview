<?php

namespace seewiki;

class session
{
    private $_data;
    private $_db;
    private $_key = "Q9NTV34NHTMUIJhjniahNUH8";
    private static $instance;
    private $_autosave = false;
    
    public function __construct()
    {
        $this->_db = database::getinstance();
    }
    
    /**
     * Singleton method used to access object avoiding duplication
     * @return object
     **/
    static function getinstance()
    {
        if(!isset(self::$instance))
        {
            self::$instance = new self();    
            session_start();
        }
        
        return self::$instance;
    }
    
    /**
     * Get the session data
     */
    private function getSession()
    {
        $query = "SELECT data FROM sessions WHERE id = :sessionid";
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->bindParam(':sessionid', session_id());
        $stmt->execute();
        $result = $stmt->fetch(); 

        if($result)
        {
            $this->_data = $this->decode($result['data']);
            return true;
        }
        
        return false;
    }
    
    /**
     * Return if a session exists
     * @return type 
     */
    public function hasSession()
    {
        return $this->getSession();
    }
    
    /**
     * Start a session
     */
    public function startSession()
    {
        $query = "INSERT INTO sessions SET id = :sessionid, 
                lastaccessed = :lastaccessed
                ON DUPLICATE KEY UPDATE lastaccessed = :lastaccessed";
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->bindParam(':sessionid', session_id());
        $stmt->bindParam(':lastaccessed', time());
        $stmt->execute();
    }
    
    
    /**
     * Get data from the session
     * @return type 
     */
    public function get($key)
    {
        if(empty($this->_data))
        {
            $this->getSession();
        }
                
        if(isset($this->_data[$key]))
        {
            return $this->_data[$key];
        }
        
        return false;
    }
    
    /**
     * End the session, remove from database and client
     */
    public function endSession()
    {
        $query = "DELETE FROM sessions WHERE id = :sessionid";
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->bindParam(':sessionid', session_id());
        $stmt->execute();
        
        session_destroy();
        
        return true;
    }
    
    /**
     * Set session data key
     * @param string $key
     * @param string $value 
     */
    public function set($key, $value)
    {
        $this->_data[$key] = $value;
        
        if($this->_autosave == true)
        {
            $this->save();
        }
    }
    
    /**
     * Persist the data in the session object by saving to database
     */
    public function save()
    {   
        $sessionId = session_id();
        $query = "UPDATE sessions SET data = :data, 
                lastaccessed = :lastaccessed
                WHERE id = :sessionid";
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->bindParam(':data', $this->encode($this->_data));
        $stmt->bindParam(':sessionid', $sessionId);
        $stmt->bindParam(':lastaccessed', time());
        $stmt->execute();
    }
    
    /**
     * Encode the data
     * @param type $value
     * @return type 
     */
    public  function encode($data)
    { 
        if(!$data){return false;}
        $string = serialize($data);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->_key, $string, MCRYPT_MODE_ECB, $iv);
        return trim(base64_encode($crypttext)); 
    }
 
    /**
     * Decode the data
     * @param type $value
     * @return type 
     */
    public function decode($data)
    {
        if(!$data){return false;}
        $crypttext = base64_decode($data); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->_key, $crypttext, MCRYPT_MODE_ECB, $iv);
        return unserialize(trim($decrypttext));
    }
}