<?php
/**
 * The Registry class
 * Implements the Registry and Singleton design patterns
 *
 * @version 0.1
 * @author David Bainbridge
 * @since 19/06/10
 * */

namespace seewiki;
use PDO;

class database
{
    public $pdo;
    private static $instance;

    private function __construct() 
    {
        // Get the database settings from the registry
        $registry = registry::getInstance();
        $settings = $registry->get('settings');
        $dbs = $settings['database'];

        try
        {
            $this->pdo = new PDO("mysql:host=" . $dbs['host'] . ";dbname=" .$dbs['database'], $dbs['username'], $dbs['password']);
            if(APPLICATION_ENV == 'development')
            {
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        }
        catch(PDOException $e)
        {
            //TODO ERROR HANDLING - USE THE ERRORS CLASS
        }
    }

    /**
     * Singleton method used to access object avoiding duplication
     * @return object
     **/
    static function getinstance()
    {
        if(!isset(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Encrypt password
     * @param string $password
     * @param string $salt
     * @return string 
     */
    public function encryptPassword($password, $salt)
    {
        return sha1($password . $salt . 'salty');
    }
    
    
    /**
     * Prevent duplication of this object
     **/
    public function __clone()
    {
        //todo using trigger_error?
        trigger_error('Cloning the registry is not permitted.',E_USER_ERROR);
    }
}