<?php

namespace seewiki;

abstract class model
{
    private $_model;
    private $_repository;    

    
    public function __construct()
    {
        $this->_model = end(explode('\\', get_called_class()));
        $this->_repository = '\\models\\repositories\\' . $this->_model;
    }
    
    /**
     * Save a model entity
     */
    public function save()
    {
        $modelRepository = new $this->_repository();
        $modelRepository->saveEntity($this);
    }
   
    
    /**
     * Special getter for the table
     * @return string 
     */
    public function getTable()
    {
        return $this->_table;
    }
    
    /**
     * Magic getter
     * @param string $var
     * @return string 
     */
    public function __get($var)
    {
        return $this->$var;
    }
}
?>
