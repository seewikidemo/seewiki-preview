<?php

namespace seewiki;
use seewiki\session;
use seewiki\access;

class baseController 
{
    protected $_controller;
    protected $_action;
    protected $_params;
    protected $_view;
    
    public function __construct($params, $view)
    {
        $this->_controller = $params['controller'];
        $this->_action = $params['action'];
        $this->_params = $params;
        $this->_view = $view;
        
        // Pass the controller to the view for js loading
        $this->_view->controller = $this->_controller;
        
        // Check if the user is logged in 
        $session = session::getinstance();
        if($session->hasSession() == false)
        {
            if(access::hasAccess($this->_controller, $this->_action))
            {
                header('Location: /');
                exit;
            }
        }
        else
        {
            if($this->_controller == 'index' && $this->_action == 'index')
            {
                header('Location: /wiki');
                exit;
            }
            
            $user = $session->get('user');
            $this->_view->firstName = $user['firstName'];
            
            $this->_view->searchForm = new \forms\search();
        }
    }
    
    /**
     * Change the default layout
     * @param string $layoutName 
     */
    protected function setLayout($layoutName)
    {
        $registry = registry::getInstance();
        $settings = $registry->get('settings');
        $settings['layout'] = $layoutName;
        $registry->set('settings', $settings);
    }
}

?>
