<?php

namespace seewiki;
use seewiki\error;
use seewiki\database;


/**
 * Repository abstract
 */
abstract class repository
{
    protected $_db; 
    protected $_model;
    protected $_entity;
    
    function __construct()
    {
       $this->_db = database::getinstance();
       $this->_model = $childClassName = end(explode('\\', get_called_class()));
       $modelPath = 'models\\' . $this->_model;
       $this->_entity = new $modelPath;
    }
    
    /**
     * 
     * @param type $model
     * @return repoPath 
     */
    static function getRepository($model)
    {
        $repoPath = '\models\repositories\\' . $model;
        $repository = new $repoPath;
        return $repository;
    }
    

    /**
     * Find entity
     * @param array $params 
     */
    public function find(array $params)
    {
        $sql = "SELECT * FROM " . $this->_entity->getTable();
        
        if(!empty($params))
        {
            $sql .= " WHERE ";
            $count = 0;
            foreach($params as $column=>$value)
            {
                if($count > 0)
                {
                    $sql .= ' AND ';
                }
                $sql .= mysql_real_escape_string($column) . " = '" .
                        mysql_real_escape_string($value) ."'";
                $count++;
            }
        }

        $stmt = $this->_db->pdo->query($sql);
        $entity = $stmt->fetchObject('models\\' . $this->_model);
        
        return $entity;
    }
    
    /**
     * Insert or update saved entities
     * @param mixed $entity 
     */
    public function saveEntity($entity)
    {
        if(empty($entity->id))
        {
            // Insert
            $columns = array();
            $values = array();
            foreach($entity as $key=>$value)
            {
                if($key != 'id')
                {
                    $columns[] = $key;
                }
            }

            $query = "INSERT INTO " . $entity->getTable() . " (" . mysql_real_escape_string(implode(',', $columns)) . 
                    ") VALUES (:" . mysql_real_escape_string(implode(',:', $columns)) . ")";
            $stmt = $this->_db->pdo->prepare($query);
            foreach($entity as $key=>$value)
            {
                if($key != 'id')
                {
                    $stmt->bindValue(mysql_real_escape_string($key), $value);
                }
            }            
        }
        else
        {
            // Update
            $setStrings = array();
            foreach($entity as $key=>$value)
            {
                $setStrings[] = $key . ' = :' . $key;
            }
            
            $query = "UPDATE " . $entity->getTable() . " SET " . mysql_real_escape_string(implode(',', $setStrings));
            $stmt = $this->_db->pdo->prepare($query);
            foreach($entity as $key=>$value)
            {
                $stmt->bindParam(':' . mysql_real_escape_string($key), $value);
            }

        }

        try
        {
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo "<pre>";print_r($e);echo "</pre>";exit;
        }
    }
    
    /**
     * Magic getter
     * @param string $var
     * @return string 
     */
    public function __get($var)
    {
        $var = '_' . $var;
        if(isset($this->$var))
        {
            return $this->$var;
        }
        return false;
    }
    
    /**
     * Fetch all rows in a table
     */
    public function fetchAll()
    {
        $query = "SELECT * FROM " . $this->_entity->getTable();
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->execute();
        
        $results = $stmt->fetchAll();
        return $results;
    }
    
    /*
    public function fetchAll()
    {
        try
        {      
            $query = $this->_pdo->prepare('SELECT * FROM :model', array(':model'=> $this->_model));
            $query->setFetchMode(PDO::FETCH_CLASS, '\\models\\' . $this->_model);
            $query->fetch();
             $query->saymyname();
            
            
            
           
             * WORKS
             
            //$query = $this->_pdo->prepare('SELECT * FROM :model', array(':model'=> $this->_model));
            $query = $this->_pdo->prepare('SELECT * FROM users');
            $query->execute();
            
            while($row = $query->fetch())
            {
                echo "<pre>";print_r($row);echo "</pre>";
            }
            
            echo "<pre>";print_r($result);echo "</pre>";
            // END
            
  
        }
        catch(Exception $e)
        {
            echo "<pre>";print_r($e);echo "</pre>";exit;
        }
       // echo "<pre>RESULTS:";print_r($results);echo "</pre>";exit;
        //return $results;
        
    }*/

}
