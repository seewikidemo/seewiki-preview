<?php

// Load the registry for storing variables and objects
require_once(APPLICATION_PATH . '/../library/seewiki/registry.php');
$registry = seewiki\registry::getInstance();

// Pass the url request through to the bootstrap
require_once(APPLICATION_PATH . '/bootstrap.php');
$request = ($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : 'index/index';
$bootstrap = new bootstrap($request);
$bootstrap->init();

/**
 * Auto load classes
 * @param string $class 
 */
function __autoload($class)
{
    // Ensure only requesting expected files
    $class = preg_replace("/[^a-zA-Z0-9]\/+/", "", $class);

    // Set the locations files can be included from
    $paths = array();
    $paths[0] = APPLICATION_PATH . '/../library/' . $class . '.php';
    $paths[1] = APPLICATION_PATH . '/' . $class . '.php';

    foreach($paths as $path)
    {
        if(file_exists($path))
        {
            require_once($path);
            break;
        }
    }
}

?>
