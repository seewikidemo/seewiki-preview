<?php
/**
 * View class for setting up and processing view files
 */
namespace seewiki;
use \seewiki\autoEscape;

class view 
{
    private $_viewPath;
    private $_vars;
    private $_unescapedVars;
    private $_renderLayout = true;
    private $_renderView = true;
    
    public function __construct($controller, $action)
    {
        $this->_viewPath = $viewPath = APPLICATION_PATH . '/views/' . 
                $controller . '/' . $action . '.php';
    }
    
    
    /**
     * Magic setter for view vars
     * @param string $key,
     * @param mixed $value 
     */
    public function __set($key, $value)
    {
        $this->_vars[$key] = $value;
    }
    
    /**
     * Magic getter for view variables
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        if(isset($this->_vars[$key]))
        {
            return $this->_vars[$key];
        }
        
        return false;
    }
    
    /**
     * Get unescaped variable
     * @param string $key
     * @return mixed
     */
    public function getUnescapedVar($key)
    {
        if(isset($this->_unescapedVars[$key]))
        {
            return $this->_unescapedVars[$key];
        }
        
        return false;
    }
    
    /**
     * Set if the layout should be rendered
     * @param bool $render 
     */
    public function setRenderLayout($render)
    {
        $this->_renderLayout = $render;
    }    
    
    /**
     * Set if the view should be rendered
     * @param bool $render 
     */
    public function setRenderView($render)
    {
        $this->_renderView = $render;
    }
    
    /**
     * Setup the layout and view
     */
    public function render()
    {    
        $this->autoEscape();
        
        // Include the layout
        if($this->_renderLayout == true)
        {
            $registry = registry::getinstance();
            $settings = $registry->get('settings');
            $layout = $settings['layout'];
            $layoutPath = APPLICATION_PATH . '/layouts/' . $layout . '.php';
            include($layoutPath);
        }
        elseif($this->_renderView == true)
        {
            // Just render the view
            $this->renderView();
        }
    }
    
    /**
     * Extract view variables and include the view file
     */
    public function renderView()
    {        
        // Include the view
        include($this->_viewPath);
    }
    
    /**
     * Auto-escape the view variables
     */
    public function autoEscape()
    {        
        if($this->_unescapedVars === null)
        {
            $autoEscape = new \seewiki\autoEscape();
            // Save raw variables in case need unescaped in the view
            $this->_unescapedVars = $this->_vars;
            if(!empty($this->_vars))
            {
                $this->_vars = $autoEscape->escapeDeep($this->_vars);
            }
        }
    }
}

?>