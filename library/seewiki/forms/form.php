<?php
/**
 * Form class 
 */
namespace seewiki\forms;
use seewiki\core;
use seewiki\request;
use seewiki\errors\error;

abstract class form 
{
    protected $_elements;
    protected $_action;
    protected $_formName;
    protected $_method;
    protected $_id;
    protected $_requiredFields;
    
    abstract function init();
    abstract function process();
    
    public function __construct()
    {
        $this->init();

    }
    
    /**
     * Start of processing form request
     */
    protected function validate(array $params)
    {
        $errors = false;
        foreach($this->_elements as $element)
        {
            // Check if required
            if($element->required == true && (!array_key_exists($element->name, $params) || $params[$element->name] == ''))
            {
                error::handle('warning', $element->name . ' is a required field');
                $errors = true;
            }
            
            if(isset($params[$element->name]))
            {
                // Check is longer than min length
                if($element->minLength != '' && $element->minLength > strlen($params[$element->name]))
                {
                    error::handle('warning', $element->name . ' must be longer than ' . $element->minLength . ' characters.');
                    $errors = true;
                }

                // Check is shorter than max length
                if($element->maxLength != '' && $element->maxLength < strlen($params[$element->name]))
                {
                    error::handle('warning', $element->name . ' must be shorter than ' . $element->maxLength . ' characters.');
                    $errors = true;
                }
            }
        }
        
        if($errors == true)
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Check if required fields are complete
     * @return bool 
     */
    protected function checkRequiredFields()
    {
        if(!empty($this->_requiredFields))
        {
            foreach($this->_requiredFields as $requiredField)
            {

            }
        }
        
        return true;
    }
    
    /**
     * Add an element to the form
     * @param seewiki\forms\element $element 
     */
    protected function addElement(\seewiki\forms\element $element)
    {
        $elementName = core::alnum($element->name);
        $this->_elements->$elementName = $element;
    }
    
    /**
     * Magic getter for fetching elements
     * @return string
     */
    public function __get($element)
    {
        if(property_exists($this->_elements,core::alnum($element)))
        {
            return $this->_elements->$element->render();
        }
    }
    
    /**
     * Generate a form header
     * @return string
     */
    public function generateHeader()
    {
        $header = "<form";
        if(isset($this->_method))
        {
            $header .= " method = '" . $this->_method . "'";
        }
        if(isset($this->_action))
        {
            $header .= " action = '" . $this->_action . "'";
        }
        if(isset($this->_id))
        {
            $header .= " id = '" . $this->_id . "'";
        }
        if(isset($this->_formName))
        {
            $header .= " name = '" . $this->_formName . "'";
        }
        $header .= ">";
        return $header;
    }
    
    /**
     * Generate form footer
     * @return string
     */
    public function generateFooter()
    {
        $footer = "<input type='hidden' name='formName' value='" . 
                $this->_formName . "' />";
        $footer .= "</form>";
        return $footer;
    }
}

?>
