<?php
namespace seewiki\forms;
use seewiki\core;

abstract class element
{
    protected $_name;
    protected $_id;
    protected $_alt;
    protected $_required;
    protected $_classes;
    
    /**
     * @return string
     */
    abstract function render();
    
    /**
     * Magic setter for element properties
     * @param string $var 
     */
    public function __set($key, $value)
    {
        $autoEscape = new \seewiki\autoEscape();
        $key = "_" . $key;
        if(property_exists($this, $key))
        {
            $this->$key = $autoEscape->escapeDeep($value);
        }
    }
    
    /**
     * Magic getter
     * @param string $key
     * @return mixed 
     */
    public function __get($key)
    {
        $key = '_' . $key;
        if(property_exists($this, $key))
        {
            return $this->$key;
        }
        return false;
    }
    
    /**
     * Add a class to the element
     * @param string $var 
     */
    public function addClass($var)
    {
        $this->_classes[] = core::alnum($var);
    }
}

?>
