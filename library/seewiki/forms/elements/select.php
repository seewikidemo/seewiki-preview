<?php
/**
 * Form class for managing forms
 */
namespace seewiki\forms\elements;

class select extends \seewiki\forms\element
{     
    protected $_options;
    
    public function render()
    {
        $element = "<select name='" . \seewiki\core::alnum($this->_name) . "'";
        if(isset($this->_classes))
        {
            $element .= " class='" . implode(' ', array_map("\seewiki\core::alnum", $this->_classes)) . "'";
        }
        $element .= ">";
        
        foreach($this->_options as $value=>$text)
        {
            $element .= '<option value=' . $value . '>' . $text . '</option>';
        }
        
        $element .= "</select>";
        return $element;
    }
    
    /**
     * Add an option to the drop down
     * @param string $value
     * @param string $text 
     */
    public function addOption($value, $text)
    {
        $this->_options[\seewiki\core::alnum($value)] = \seewiki\core::alnum($text); 
    }
}

?>
