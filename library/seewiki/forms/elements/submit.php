<?php
/**
 * Form element submit button
 */
namespace seewiki\forms\elements;
use seewiki\core;

class submit extends \seewiki\forms\element
{    
    
    protected $_value;
    
    public function render()
    {
        $element = "<input type='submit' name='" . $this->_name . "'";
        if(isset($this->_alt))
        {
            $element .= " alt='" . $this->_alt . "' ";
        }
        if(isset($this->_classes))
        {
            $element .= " class='" . implode(' ', $this->_classes) . "'";
        }
        if(isset($this->_value))
        {
            $element .= " value='" . $this->_value . "' ";
        }
        $element .= "/>";
        
        return $element;
    }
}

?>
