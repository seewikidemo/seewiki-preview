<?php
/**
 * Form class for managing forms
 */
namespace seewiki\forms\elements;

class textarea extends \seewiki\forms\element
{         
    public function render()
    {
        $element = "<textarea name='" . \seewiki\core::alnum($this->_name) . "'";
        if(isset($this->_classes))
        {
            $element .= " class='" . implode(' ', array_map("\seewiki\core::alnum", $this->_classes)) . "'";
        }
        $element .= "></textarea>";
        
        return $element;
    }
}

?>
