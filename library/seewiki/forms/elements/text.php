<?php
/**
 * Form class for managing forms
 */
namespace seewiki\forms\elements;

class text extends \seewiki\forms\element
{     
    protected $_minLength;
    protected $_maxLength;
    
    public function render()
    {
        $element = "<input type='text' name='" . \seewiki\core::alnum($this->_name) . "'";
        if(isset($this->_alt))
        {
            $element .= " alt='" . \seewiki\core::alnum($this->_alt) . "' ";
        }
        if(isset($this->_classes))
        {
            $element .= " class='" . implode(' ', array_map("\seewiki\core::alnum", $this->_classes)) . "'";
        }
        $element .= "/>";
        
        return $element;
    }
}

?>
