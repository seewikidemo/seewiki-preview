<?php
namespace seewiki\errors;

class errorFatal extends errorAbstract
{
    /**
     * Handle the error
     * @param type $error 
     */
    public function handle($error)
    {
        $this->logError($error);
        switch($error->getCode())
        {
            case 404:
                $redirect = 'notfound';
                break;
            default:
                $redirect = 'applicationerror';
        }
        $this->redirect($redirect);
    }
}
?>
