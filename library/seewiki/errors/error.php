<?php

namespace seewiki\errors;

class error
{
    /**
     * Handle errors - factory generation of error type
     * @param string $type
     * @param mixed $e
     */
    public static function handle($type, $e)
    {
        $errorClass = 'seewiki\errors\error' . ucfirst($type);
        $error = new $errorClass();
        $error->handle($e);
    }
}
?>
