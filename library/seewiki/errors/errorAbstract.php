<?php

namespace seewiki\errors;

abstract class errorAbstract
{
    abstract protected function handle($error);
    
    protected function logError()
    {
        
    }
    
    /**
     * Redirect to appropriate error page
     * @param string $errorType 
     */
    protected function redirect($errorType)
    {
        header('Location: /err/' . $errorType);
    }
}
?>
