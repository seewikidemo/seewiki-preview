<?php
namespace seewiki\errors;
use seewiki\notifications\notification;

class errorWarning extends errorAbstract
{
    /**
     * Handle the error
     * @param string $message
     * @param integer $code
     * @param type $error 
     */
    public function handle($message)
    {
        notification::addNotification($message, 'Warning');
    }
}
?>
