<?php
/**
 * 
 * Set which pages don't require security
 * 
 */
namespace seewiki;

class access 
{
    public static function hasAccess($controller, $action)
    {
        $access = array();
        $access[] = array('index>index');
        $access[] = array('index>test');
        $access[] = array('user>login');
        $access[] = array('user>logout');
        $access[] = array('user>register');
        
        if(in_array($controller . '>' . $action, $access))       
        {
            return true;
        }
        
        return false;        
    }
}

?>
