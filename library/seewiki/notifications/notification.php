<?php

namespace seewiki\notifications;
use seewiki\registry;

class notification
{
    /**
     * Add a notification to the registry
     * @param string $message
     * @param string $type 
     */
    public static function addNotification($message, $type)
    {
        $notifications = array();
        $registry = registry::getInstance();
        $settings = $registry->get('settings');
        
        if($registry->isRegistered('notifications'))
        {
            $notifications = $registry->get('notifications');
        }
        
        // Create a new notification event
        $notification = new event();
        $notification->setMessage($message);
        $notification->setType($type);
        
        // Add it to the notifications array in the registry
        $notifications[] = $notification;
        $registry->set('notifications', $notifications);
        
    }
    
    /**
     * Get notifications
     * @return type 
     */
    public static function getNotifications()
    {
        $registry = registry::getInstance();
        if($registry->isRegistered('notifications'))
        {
            $notifications = $registry->get('notifications');
            return $notifications;
        }
        else
        {
            return array();
        }
    }
  
}
?>
