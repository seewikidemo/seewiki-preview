<?php

namespace seewiki\notifications;

class event
{
    private $_type = 'Info';
    private $_message;
    
    public function getType()
    {
        return $this->_type;
    }
    
    public function getMessage()
    {
        return $this->_message;
    }
    
    public function setType($type)
    {
        $this->_type = ucfirst($type);
    }
    
    public function setMessage($message)
    {
        $this->_message = $message;
    }
}

?>
