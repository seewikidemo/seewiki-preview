<?php
namespace controllers;
use seewiki\request;
use seewiki\ajaxResponse;
use seewiki\notifications\notification;

class user extends \seewiki\baseController
{
    public function loginAction()
    {
        $this->_view->setRenderLayout(false);
        $loginForm = new \forms\login();
        
        $this->_view->loginForm = $loginForm;

        if(request::post() == true)
        {
            $this->_view->setRenderView(false);
            $success = $loginForm->process();
            
            $ajaxResponse = new ajaxResponse();
            $ajaxResponse->setSuccess($success);
            $ajaxResponse->outputResponse();
        }
    }
    
    /**
     * Log the user out
     */
    public function logoutAction()
    {
        $userService = new \services\user();
        $userService->logout();
        header('location: /');
    }
    
    /**
     * Registration form called in with ajax
     */
    public function registerAction()
    {
        $this->_view->setRenderLayout(false);
        $registerForm = new \forms\register();
        
        $this->_view->registerForm = $registerForm;

        if(request::post() == true)
        {
            $this->_view->setRenderView(false);
            $success = $registerForm->process();
            
            $ajaxResponse = new ajaxResponse();
            $ajaxResponse->setSuccess($success);
            $ajaxResponse->outputResponse();
        }
    }
}

?>
