<?php
namespace controllers;

class wiki extends \seewiki\baseController
{
    
    public function indexAction()
    {
        $loginForm = new \forms\entry();
        $this->_view->entryForm = $loginForm;        
    }
    
    /**
     * Get form for adding a category via ajax (no layout rendering)
     */
    public function addcategoryAction()
    {
        $this->_view->setRenderLayout(false);
        $addCategoryForm = new \forms\addCategory();
        $this->_view->addCategoryForm = $addCategoryForm; 
        
        if(request::post() == true)
        {
            $this->_view->setRenderView(false);
            $success = $addCategoryForm->process();
            
            $ajaxResponse = new ajaxResponse();
            $ajaxResponse->setSuccess($success);
            $ajaxResponse->outputResponse();
        }
    }
}

?>
