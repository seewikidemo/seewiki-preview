<?php
namespace controllers;

class index extends \seewiki\baseController
{
    public function __construct($params, $view)
    {
        $this->setLayout('splash');
        parent::__construct($params, $view);
    }
    
    public function indexAction()
    {
    }
    
    public function testAction()
    {
        $this->_view->setRenderLayout(false);
        $this->_view->setRenderView(false);
    }
}

?>
