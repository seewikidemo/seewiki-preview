<?php

namespace models\repositories;
use seewiki\repository;

class category extends repository
{

    /**
     * Get categories by user id
     * @param integer $userId
     * @return array 
     */
    public function getCategoriesByUserId($userId)
    {
        $query = "SELECT * FROM categories WHERE user_id = :userId";
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->bindParam(':userId', $userId);
        $stmt->execute();
        
        $results = $stmt->fetchAll();
        
        return $results;
    }
}

?>
