<?php

namespace models\repositories;
use seewiki\repository;

class user extends repository
{
    /**
     * Check if a user exists
     * @param string $username
     * @return bool
     */
    public function userExists($username)
    {
        $query = "SELECT count(*) FROM users WHERE username = :username";
        $stmt = $this->_db->pdo->prepare($query);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        
        $result = $stmt->fetchColumn();

        if($result > 0)
        {
            return true;
        }
        
        return false;
    }
}

?>
