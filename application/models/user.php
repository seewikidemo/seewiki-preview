<?php

namespace models;
use seewiki\model;

class user extends model
{
    protected $_table = 'users';
    
    public $id;
    public $username;
    public $firstname;
    public $lastname;
    public $password;
    public $salt;
    
    public function saymyname()
    {
        echo $this->firstname . " " . $this->lastname ."<br>";
    }
}
?>
