<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="/css/site.css" media="screen" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 
        <script type="text/javascript" src="/js/core.js"></script> 
        <script type="text/javascript" src="/js/site.js"></script> 
        

        <title>Seewiki.net</title>
    </head>
    <body id="<?php echo $this->controller; ?>" class="global">
        
        <div class="headingBackground"></div>
        <div class="container">
            <header>
                <div class="heading">
                    <h1>SEEWIKI</h1>
                    <div class="domain">.NET</div>
                </div>
                <nav>
                    <ul>
                        <li>Logged in as <?php echo $this->firstName; ?></li>
                        <li><a href="/user/logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
            
            <div class="search">
                <div class="alignLeft">
                    <?php echo $this->searchForm->keyword; ?>
                </div>
                <div class="alignLeft">
                    <?php echo $this->searchForm->submit; ?>
                </div>
            </div>  
            
            <div class="options alignLeft">
                <input type="button" class="button" id="addEntry" value="Add Entry" />
            </div>
            
            <div class="clear tenSplitter"></div>
            <div id="messagesContainer"></div>
            <div id="formInjection"></div>
            <div class="clear"></div>
            <section>
                <?php
                    $this->renderView();
                ?>
            </section>
        </div>
        <div class="clear tenSplitter"></div>
        <footer>
            &copy; <a href="http://www.activelab.co.uk">Active lab</a> 2013. All rights reserved.
        </footer>
    </body>
</html>