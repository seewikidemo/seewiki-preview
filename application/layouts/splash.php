<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="/css/site.css" media="screen" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 
        <script type="text/javascript" src="/js/core.js"></script> 
        <script type="text/javascript" src="/js/site.js"></script> 
        

        <title>Seewiki.net</title>
    </head>
    <body id="<?php echo $this->controller; ?>" class="splash">
        
        <div class="headingBackground">
            <div class="textTexture hidden"></div>
        </div>
        <div class="container">
            <header>
                <div class="heading">
                    <div class="slogan">WHEN YOU CAN'T REMEMBER...</div>
                    <h1>SEEWIKI</h1>
                    <div class="domain">.NET</div>
                </div>
                <nav>
                    <ul>
                        <li><a id="login">Login</a></li>
                        <li><a id="register" class="createAccount">Register</a></li>
                    </ul>
                </nav>
            </header>
            <div class="clear thirtySplitter"></div>
            <div id="messagesContainer"></div>
            <div id="formInjection"></div>
            <section>
                <?php
                    $this->renderView();
                ?>
            </section>
        </div>
        <div class="clear tenSplitter"></div>
        <footer>
            &copy; <a href="http://www.activelab.co.uk">Active lab</a> 2013. All rights reserved.
        </footer>
    </body>
</html>