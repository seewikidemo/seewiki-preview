
<div id="addEntryContainer" class="addEntryContainer hidden">
    <h2>Add Entry</h2>
    <div class="formElement">
        <label for="type">Type:</label> <br/>
        <?php echo $this->entryForm->type; ?>
    </div>
    <div class="formElement">
        <label for="category">Category:</label> <br/>
        <?php echo $this->entryForm->category; ?>
        <a id="addCategory" class="clickable">+ Add Category</a>
    </div>
    <div class="formElement">
        <label for="type">Title:</label> <br/>
        <?php echo $this->entryForm->title; ?>
    </div>
    <div class="formElement">
        <label for="entry">Entry:</label> <br/>
        <?php echo $this->entryForm->entry; ?>
    </div>
</div>
<h2>Categories</h2>
!categories!