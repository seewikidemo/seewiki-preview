<div class="tenSplitter"></div>
<div class="formContainer registerForm contentContainer">
    <?php echo $this->loginForm->generateHeader(); ?>
    <div class="core">
        <h2>Add Category</h2>
        <div class="formElement">
            <label for="name">Name:</label><br/>
            <?php echo $this->addCategoryForm->name; ?>
        </div>
        <div class="formElement">
            <label for="parent">Parent Category:</label><br/>
            <?php echo $this->addCategoryForm->parent; ?>
        </div>
        <div class="tenSplitter clear"></div>
        <div class="formElement">
            <div class="alignLeft">
                <?php echo $this->addCategoryForm->submit; ?>
            </div>
        </div>
    </div>
    <?php echo $this->loginForm->generateFooter(); ?>
    <div class="clear"></div>
</div>
<div class="tenSplitter"></div>