<div class="infoContainer">
    <div class="tenSplitter"></div>
    <div class="infoBox">
        <div class="alignLeft">
            <img src="/img/infoIcon.png" alt="info icon" class="icon" />
        </div>
        <div class="information alignLeft">
            Save your notes, ideas, links to videos or music and more
            in your personal online wiki!
        </div>
    </div>
    <div class="clear tenSplitter"></div>
    <div class="infoBox">
        <div class="alignLeft">
            <img src="/img/laptopIcon.png" alt="laptop icon" class="icon" />
        </div>
        <div class="information alignLeft">
            Access anywhere and everywhere; optimised for laptops, tablets and mobiles.
        </div>
    </div>
    <div id="createAccountContainer" class="createAccountBox alignRight">
        <div class="arrow"></div>
        <a id="createAccount" class="button createAccount">Create a free account</a>
    </div>
</div>