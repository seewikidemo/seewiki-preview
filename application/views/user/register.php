<div class="tenSplitter"></div>
<div class="formContainer registerForm">
    <div class="core">
        <h2>Register</h2>
        <?php echo $this->registerForm->generateHeader(); ?>
            <div class="formElement">
                <label for="firstName">First Name:</label><br/>
                <?php echo $this->registerForm->firstName; ?>
            </div>
            <div class="formElement">
                <label for="lastName">Last Name:</label><br/>
                <?php echo $this->registerForm->lastName; ?>
            </div>
            <div class="formElement">
                <label for="email">Email (username):</label><br/>
                <?php echo $this->registerForm->email; ?>
            </div>
            <div class="formElement">
                <label for="password">Password:</label><br/>
                <?php echo $this->registerForm->password; ?>
            </div>
            <div class="tenSplitter clear"></div>
            <div class="formElement">
                <div class="alignLeft">
                    <?php echo $this->registerForm->submit; ?>
                </div>
            </div>
        <?php echo $this->registerForm->generateFooter(); ?>
    </div>
    <div class="extra">
        <div class="tips">
            <p>
                Creating an account is completely free!
                Sign up and get instant access on all your devices.
            </p>
        </div>
    </div>
</div>

<div class="thankYouContainer hidden">
    <h2>Registration complete</h2>
    <p>
        Welcome to seewiki! 
        You can now login using your username and password.
    </p>
</div>