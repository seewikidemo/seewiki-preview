<div class="tenSplitter"></div>
<div class="formContainer registerForm">
    <div class="core">
        <h2>Login</h2>
        <?php echo $this->loginForm->generateHeader(); ?>
            <div class="formElement">
                <label for="username">Username (email):</label><br/>
                <?php echo $this->loginForm->username; ?>
            </div>
            <div class="formElement">
                <label for="password">Password:</label><br/>
                <?php echo $this->loginForm->password; ?>
            </div>
            <div class="tenSplitter clear"></div>
            <div class="formElement">
                <div class="alignLeft">
                    <?php echo $this->loginForm->submit; ?>
                </div>
            </div>
        <?php echo $this->loginForm->generateFooter(); ?>
    </div>
    <div class="extra">
        <div class="tips">
            <p>
                Use seewiki on all your devices!
                Seewiki is compatible with tablets and mobiles.
            </p>
        </div>
    </div>
</div>