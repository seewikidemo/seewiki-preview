<?php

class bootstrap
{
    private $_request;
    
    public function __construct($request)
    {
        $this->_request = trim($request, '/');
    }
    
    public function init()
    {
        // Process the application.ini file to get settings
        $settings = parse_ini_file(APPLICATION_PATH . '/configs/application.ini', true);
        $globalSettings = array_shift($settings);
        foreach($settings as $settingGroup=>$groupSettings)
        {
            if(isset($groupSettings['domains']) && in_array($_SERVER['SERVER_NAME'], $groupSettings['domains']))
            {
                define('APPLICATION_ENV', $settingGroup);
                $mergedSettings = array_merge($globalSettings, $groupSettings);
                break;
            }
        }

        $registry = \seewiki\registry::getinstance();
        $registry->set('settings', $mergedSettings);
        
        // Process the requested url
        $this->processRequest();
    }
    
    /**
     * Process url request
     */
    private function processRequest()
    {     
        // Split the url into requested parts
        $request = $this->alnum(explode("/" , $this->_request));

        // Controller first part of the url
        $controller = array_shift($request);
        // Default to index if none set
        $controller = ($controller == '') ? 'index' : strtolower($controller);
        // Action second part of url
        $action = array_shift($request);
        // Default to index action if none set
        $action = ($action == '') ? 'index' : $action;

        
        // Remaining array passed as the params
        $key = '';
        $params = array('controller'=>$controller,'action'=>$action);
        foreach($request as $param)
        {
            if(!$key == '')
            {
                $params[$key] = $param;
            }
            else
            {
                $key = $param;
            }
        }

        // Load the controller, action and pass through the parameters
        $controllerName = '\controllers\\' . $controller;

        if(class_exists($controllerName,true))
        {
            // Create the view 
            $view = new \seewiki\view($controller, $action);
            $dispatch = new $controllerName($params, $view);
       
            if(method_exists($dispatch,  $action . 'Action'))
            {
                call_user_func_array(array($dispatch, $action . 'Action'), array());
                call_user_func_array(array($view, 'render'), array());  
            }
            else
            {
                throw new Exception('Page Not Found', 404);
            }
        }
        else
        {
           
            throw new Exception('Page Not Found', 404);
        }
    }
    
    /**
     * Return alphanumerical string / array
     * @param mixed $string
     * @return string 
     */
    private function alnum($vars)
    {
        if(is_array($vars))
        {
            foreach($vars as $key=>$value)
            {
                $vars[$key] = $this->alnum($value);
            }
        }
        return preg_replace("/[^a-zA-Z0-9]+/", "", $vars);
    }
    
    
}
?>
