<?php
/**
 * Wiki service
 */

namespace services;
use seewiki\notifications\notification;
use seewiki\repository;
use seewiki\database;
use seewiki\session;

class wiki
{
    /**
     * Get the categories for the active user
     * @return type 
     */
    public function getCategories()
    {
        $session = session::getinstance();
        $user = $session->get('user');
        $categoryRepository = new \models\repositories\category();
        $categories = $categoryRepository->getCategoriesByUserId($user['id']);
        return $categories;
    }
    
    /**
     * Get all available entry types
     * @return type 
     */
    public function getEntryTypes()
    {
        $entryTypesRepository = new \models\repositories\entryTypes();
        $entryTypes = $entryTypesRepository->fetchAll();
        return $entryTypes;
    }
}

?>
