<?php
/**
 * User service
 */

namespace services;
use seewiki\notifications\notification;
use seewiki\repository;
use seewiki\database;
use seewiki\session;

class user 
{
    /**
     * Authenticate a user
     * @param string $username
     * @param string $password 
     */
    public function authenticate($username, $password)
    {                 
        $user = repository::getRepository('user')->find(array('username'=>$username));
        
        if(empty($user))
        {
            $message = 'User not found, please try again.';
            notification::addNotification($message, 'warning');
        }
        else
        {
            $db = database::getinstance();

            if($db::encryptPassword($password, $user->salt) != $user->password)
            {
                $message = 'Incorrect password, please try again.';
                notification::addNotification($message, 'warning');
            }
            else
            {
                $session = session::getinstance();
                $session->startSession();
                $session->set('user', array('id' => $user->id,
                                            'firstName' => $user->firstname,
                                            'lastName' => $user->lastname));
                $session->save();
                return true;
            }
        }
        return false;
    }
    
    /**
     * Log the user out
     */
    public function logout()
    {
        $session = session::getinstance();
        $session->endSession();
    }
    
    /**
     * Create a new user
     * @param array $user 
     */
    public function createUser($user)
    {   
        // Check if the username is taken
        $userRepository = new \models\repositories\user();
        $userExists = $userRepository->userExists($user['email']);
        
        if($userExists == true)
        {
            $message = 'Sorry - this email address has already been used!';
            notification::addNotification($message, 'Warning');
        }
        else
        {
            try
            {
                $salt = uniqid();

                $userModel = new \models\user();
                $userModel->username = $user['email'];
                $userModel->firstname = $user['firstName'];
                $userModel->lastname = $user['lastName'];
                $userModel->salt = $salt;
                $userModel->password = sha1($user['password'] . $salt . 'salty');
                $userModel->save();

                return true;
            }
            catch(Exception $e)
            {
                error::handle('fatal', $e);
            }
        }
    }
}

?>
