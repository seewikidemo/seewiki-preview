<?php
namespace forms;
use seewiki\forms;
use seewiki\request;

class login extends forms\form
{
    public function init()
    {
        $this->_requiredFields = array('username', 'password');
        
        $this->_id = 'login';
        $this->_formName = 'login';
        $this->_action = 'user/login';
        
        // Username
        $element = new forms\elements\text();
        $element->name = 'username';
        $element->id = 'username';
        $element->required = true;
        $this->addElement($element);
        
        // Password
        $element = new forms\elements\password();
        $element->name = 'password';
        $element->id = 'password';
        $element->required = true;
        $this->addElement($element);
        
        // Submit
        $element = new forms\elements\button();
        $element->name = 'submit';
        $element->id = 'submit';
        $element->value = "Login";
        $element->addClass('button');
        $element->addClass('ajaxSubmit');
        $this->addElement($element);
    }
    
    public function process()
    {
        // Check required
        $params = request::getParams();
        $valid = $this->validate($params);
        
        if($valid == true)
        {
            $userService = new \services\user();
            return $userService->authenticate($params['username'], $params['password']);
        }
        
        return false;
    }
}

?>
