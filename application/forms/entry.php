<?php
namespace forms;
use seewiki\forms;
use seewiki\request;

class entry extends forms\form
{
    public function init()
    {
        $this->_requiredFields = array('type', 'category', 'entry');
        
        $this->_id = 'entryForm';
        $this->_formName = 'entryForm';
        $this->_action = 'user/login';
              
        // Title
        $element = new forms\elements\text();
        $element->name = 'title';
        $element->id = 'title';
        $element->required = true;
        $this->addElement($element);
        
        // Entry Type
        $element = new forms\elements\select();
        $element->name = 'type';
        $element->id = 'type';
        
        // Get the category options
        $wikiService = new \services\wiki();
        $entryTypes = $wikiService->getEntryTypes();
        
        if($entryTypes)
        {
            foreach($entryTypes as $entryType)
            {
                $element->addOption($entryType['id'], $entryType['name']);
            }
        }
                
        $element->required = true;
        $this->addElement($element);
        
        // Category
        $element = new forms\elements\select();
        $element->name = 'category';
        $element->id = 'category';
        
        // Get the category options
        $wikiService = new \services\wiki();
        $categories = $wikiService->getCategories();
        
        $element->addOption('',' -- Select Category -- ');
        if($categories)
        {
            foreach($categories as $category)
            {
                $element->addOption($category['id'], $category['name']);
            }
        }
                
        $element->required = true;
        $this->addElement($element);
           
        // Entry
        $element = new forms\elements\textarea();
        $element->name = 'entry';
        $element->id = 'entry';
        $element->required = true;
        $element->addClass('entry');
        $this->addElement($element);
        
        // Submit
        $element = new forms\elements\button();
        $element->name = 'submit';
        $element->id = 'submit';
        $element->value = "Login";
        $element->addClass('button');
        $element->addClass('ajaxSubmit');
        $this->addElement($element);
    }
    
    public function process()
    {
        // Check required
        $params = request::getParams();
        $valid = $this->validate($params);
        
        if($valid == true)
        {
            $userService = new \services\user();
            return $userService->authenticate($params['username'], $params['password']);
        }
        
        return false;
    }
}

?>
