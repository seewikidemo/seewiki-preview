<?php
namespace forms;
use seewiki\forms;
use seewiki\request;

class search extends forms\form
{
    public function init()
    {
        $this->_requiredFields = array('keyword');
        
        $this->_id = 'search';
        $this->_formName = 'search';
        $this->_action = 'wiki/search';
        
        $element = new forms\elements\text();
        $element->name = 'keyword';
        $element->id = 'keyword';
        $element->required = true;
        $this->addElement($element);
              
        // Submit
        $element = new forms\elements\button();
        $element->name = 'submit';
        $element->id = 'submit';
        $element->value = 'Search';
        $element->addClass('button');
        $element->addClass('ajaxSubmit');
        $this->addElement($element);
    }
    
    public function process()
    {
        // Check required
        $params = request::getParams();
        $valid = $this->validate($params);
        
        if($valid == true)
        {
            $wikiService = new \services\wiki();
            return $wikiService->search($keyword);
        }
        
        return false;
    }
}

?>
