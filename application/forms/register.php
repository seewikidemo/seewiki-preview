<?php
namespace forms;
use seewiki\forms;
use seewiki\request;

class register extends forms\form
{
    public function init()
    {
        $this->_requiredFields = array('firstName', 'username', 'password');
        
        $this->_id = 'register';
        $this->_formName = 'register';
        $this->_action = 'user/register';
        
        $element = new forms\elements\text();
        $element->name = 'firstName';
        $element->id = 'firstName';
        $element->required = true;
        $this->addElement($element);
        
        // Last name
        $element = new forms\elements\text();
        $element->name = 'lastName';
        $element->id = 'lastName';
        $this->addElement($element);
        
        // Email (and username)
        $element = new forms\elements\text();
        $element->name = 'email';
        $element->id = 'email';
        $element->required = true;
        $this->addElement($element);
        
        // Password
        $element = new forms\elements\password();
        $element->name = 'password';
        $element->id = 'password';
        $element->required = true;
        $this->addElement($element);
        
        // Submit
        $element = new forms\elements\button();
        $element->name = 'submit';
        $element->id = 'submit';
        $element->value = "Let's go!";
        $element->addClass('button');
        $element->addClass('ajaxSubmit');
        $this->addElement($element);
    }
    
    public function process()
    {
        // Check required
        $params = request::getParams();
        $valid = $this->validate($params);
        
        if($valid == true)
        {
            $userService = new \services\user();
            return $userService->createUser($params);
        }
        
        return false;
    }
}

?>
